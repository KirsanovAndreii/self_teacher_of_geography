package com.example.ankir.self_teacherofgeography;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

/**
 * Created by ankir on 20.06.2017.
 */

public class Retrofit {
    private static final String ENDPOINT = "https://restcountries.eu";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/rest/v1/all")
        void getCountries(Callback<List<ResponseCountries>> callback);
    }

    public static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCountries(Callback<List<ResponseCountries>> callback) {
        apiInterface.getCountries(callback);
    }
}

