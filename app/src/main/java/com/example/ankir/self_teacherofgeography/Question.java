package com.example.ankir.self_teacherofgeography;

/**
 * Created by ankir on 23.06.2017.
 */

public class Question {
    int idQuestion;
    int idLogin;
    int idCountry;
    int numberQuestion;
    int rezult;

    @Override
    public String toString() {
        return "Question{" +
                "idQuestion=" + idQuestion +
                ", idLogin=" + idLogin +
                ", idCountry=" + idCountry +
                ", numberQuestion=" + numberQuestion +
                ", rezult=" + rezult +
                '}';
    }
}
