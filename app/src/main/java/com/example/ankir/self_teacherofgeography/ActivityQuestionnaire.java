package com.example.ankir.self_teacherofgeography;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ActivityQuestionnaire extends AppCompatActivity {
    Intent intent;
    public static int currentLogin = 0; // по этому id поиск вопросов
    public static List<ResponseCountries> listAllCountries = new ArrayList<>();
    DataBaseMaster dbMaster;
    public static final int questionID_1 = 1;
    public static final String question_1 = "Call the capital of ";
    public static final int questionID_2 = 2;
    public static final String question_2 = "Call the population of ";
    public static final int questionID_3 = 3;
    public static final String question_3 = "Call the region of ";
    public static final int questionID_4 = 4;
    public static final String question_4 = " is the capital of which country?";
    List<Question> listQuestionCurrentAll;
    Random rand = new Random();
    Question question;

    @BindView(R.id.aq_qestion)
    TextView questionView;
    @BindView(R.id.aq_answer)
    EditText answerEdit;
    @BindView(R.id.aq_correct_answer)
    TextView correctAnswerView;
    @BindView(R.id.aq_check_answer)
    Button buttonCheckAnswer;
    @BindView(R.id.aq_total)
    TextView total_view;
    @BindView(R.id.aq_correct)
    TextView correct_view;
    @BindView(R.id.aq_wrong)
    TextView wrong_view;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);
        ButterKnife.bind(this);
        dbMaster = DataBaseMaster.getInstance(this);
        buttonCheckAnswer.setEnabled(false);

        Log.d("СУЩЕСТВУЕТ БАЗА?  ", "ЗАПРОС");
        if (dbMaster.getAllRegion().size() == 0) {// если размер = 0   - база пустая
            Log.d("СУЩЕСТВУЕТ БАЗА?  ", "НЕТ");
            loadingAndSaveBaseCountries();
        } else {  // можно удалить else
            Log.d("СУЩЕСТВУЕТ БАЗА?  ", "ДА");
        }
        Log.d("ПРОДОЛЖЕНИЕ  ", "ПРОДОЛЖЕНИЕ");


        intent = new Intent(ActivityQuestionnaire.this, ActivityLogIn.class);
        startActivity(intent);   //  запрос login / регистрация
        Toast.makeText(ActivityQuestionnaire.this, "id =" + currentLogin, Toast.LENGTH_SHORT).show();

    }

    @OnClick(R.id.aq_check_next)
    public void onClickNext() {
        buttonCheckAnswer.setEnabled(true);
        answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite));
        int rezultCorrect = 0; //правильные ответы
        int rezultWrong = 0; //неправильные ответы
        correctAnswerView.setText("");
        answerEdit.setText("");
        listQuestionCurrentAll = dbMaster.getQuestionsForUser(currentLogin);

        List<Question> listNoRezult = new ArrayList<>(); // неотвеченные
        for (Question question : listQuestionCurrentAll) {
            if (question.rezult == 0) listNoRezult.add(question);
            else if (question.rezult == 1) {
                rezultCorrect++;
            } else {
                rezultWrong++;
            }
        }
        if (listNoRezult.size() != 0) { // если еще есть вопросы:
            question = listNoRezult.get(rand.nextInt(listNoRezult.size()));
            total_view.setText(listQuestionCurrentAll.size() + "");
            correct_view.setText(rezultCorrect + "");
            wrong_view.setText(rezultWrong + "");
            if (question.numberQuestion == 4) {
                questionView.setText(
                        dbMaster.getCountryById(question.idCountry).capital + question_4);
            } else {
                if (question.numberQuestion == 1) {
                    questionView.setText(
                            question_1 + dbMaster.getCountryById(question.idCountry).name);
                } else if (question.numberQuestion == 2) {
                    questionView.setText(
                            question_2 + dbMaster.getCountryById(question.idCountry).name);
                } else {
                    questionView.setText(
                            question_3 + dbMaster.getCountryById(question.idCountry).name);
                }
            }
        } else {
            questionView.setText("Your knowledge is great!");
            buttonCheckAnswer.setEnabled(false);
            for (Question questions : listQuestionCurrentAll) {
                dbMaster.upDateQestionTable(questions.idQuestion, 0);
            }
        }
    }

    @OnClick(R.id.aq_check_answer)
    public void onClickCheckAnswer() {
        buttonCheckAnswer.setEnabled(false);
        String answer = answerEdit.getText().toString();
        String correctAnswer;
        if (question.numberQuestion == 4) {
            correctAnswer = dbMaster.getCountryById(question.idCountry).name;
            correctAnswerView.setText(correctAnswer);
            if (answer.equals(correctAnswer)) {
                dbMaster.upDateQestionTable(question.idQuestion, 1);
                answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen));
            } else {
                dbMaster.upDateQestionTable(question.idQuestion, -1);
                answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
            }

        } else {
            if (question.numberQuestion == 1) {
                correctAnswer = dbMaster.getCountryById(question.idCountry).capital;
                correctAnswerView.setText(correctAnswer);
                if (answer.equals(correctAnswer)) {
                    dbMaster.upDateQestionTable(question.idQuestion, 1);
                    answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen));
                } else {
                    dbMaster.upDateQestionTable(question.idQuestion, -1);
                    answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
                }
            } else if (question.numberQuestion == 2) {
                int pop = dbMaster.getCountryById(question.idCountry).population;
                int answerInt = -1;
                try {
                    answerInt = Integer.parseInt(answer);
                } catch (Exception e) {
                    answerEdit.setText("not Number!!!!!!!!!");
                }

                correctAnswerView.setText(pop + "");

                if (answerInt < pop * 1.1 && answerInt > pop * 0.9) {
                    dbMaster.upDateQestionTable(question.idQuestion, 1);
                    answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen));
                } else {
                    dbMaster.upDateQestionTable(question.idQuestion, -1);
                    answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
                }
            } else {
                correctAnswer = dbMaster.getCountryById(question.idCountry).region;
                correctAnswerView.setText(dbMaster.getRegionById(Integer.parseInt(correctAnswer)));
                if (dbMaster.getIdRegion(answer)
                        == Integer.parseInt(correctAnswer)) {
                    dbMaster.upDateQestionTable(question.idQuestion, 1);
                    answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorGreen));
                } else {
                    dbMaster.upDateQestionTable(question.idQuestion, -1);
                    answerEdit.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
                }
            }
        }

        List<Question> ls = dbMaster.getQuestionsForUser(question.idLogin);
        for (Question l : ls) {
            Log.d("REZULT" + question.idQuestion, l.toString());
        }
    }


    private void loadingAndSaveBaseCountries() {
        // Retrofit
        Retrofit.getCountries(new Callback<List<ResponseCountries>>() {
            @Override
            public void success(List<ResponseCountries> responseCountries, Response response) {
                listAllCountries.addAll(responseCountries);
                Toast.makeText(ActivityQuestionnaire.this, "Couttry load successful", Toast.LENGTH_SHORT).show();

                Log.d("add region  begin", "==============================");
                for (int i = 0; i < listAllCountries.size(); i++) {
                    dbMaster.insertRegion(listAllCountries.get(i));
                }

                int j = 1;
                List<String> listRegion = dbMaster.getAllRegion();
                for (String region : listRegion) {
                    Log.d("add region  " + j, region);
                    j++;
                }

                Log.d("add SUBregion  begin", "================================");
                for (int i = 0; i < listAllCountries.size(); i++) {
                    dbMaster.insertSubRegion(listAllCountries.get(i));
                }
                j = 1;
                List<String> listSubRegion = dbMaster.getAllSubRegion();
                for (String subregion : listSubRegion) {
                    Log.d("add SUBregion  " + j, subregion);
                    j++;
                }
                Log.d("add Countries  begin", "==================================");
                for (int i = 0; i < listAllCountries.size(); i++) {
                    dbMaster.insertCountry(listAllCountries.get(i));
                }
                j = 1;
                List<ResponseCountries> listCountry = dbMaster.getCountries(-1); // -1 запрос по всем субрегионам
                for (ResponseCountries countries : listCountry) {
                    Log.d("Country add " + j, countries.toString());
                    j++;
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(ActivityQuestionnaire.this, "errorRetrofit", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
