package com.example.ankir.self_teacherofgeography;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.ankir.self_teacherofgeography.DataBaseCreator.QuestionAndRezult;

import java.util.ArrayList;
import java.util.List;

import static android.provider.BaseColumns._ID;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.COUNTRY_CAPITAL;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.COUNTRY_NAME;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.COUNTRY_POPULATION;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.ID_SUBREGION;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.TABLE_COUNTRY;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.SubRegion.REGION_ID;

/**
 * Created by ankir on 13.06.2017.
 */

public class DataBaseMaster {
    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;

    private static DataBaseMaster instance;

    private DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }

    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }

    // таблица Users------------------------------
    public long insertUser(User user) {
        ContentValues cv = new ContentValues();
        cv.put(DataBaseCreator.User.USER_LOGIN, user.login);
        cv.put(DataBaseCreator.User.USER_PASS, user.pass);
        cv.put(DataBaseCreator.User.USER_COUNTRY, user.country);
        return database.insert(DataBaseCreator.User.TABLE_USERS, null, cv);
    }

    // выбираю одну строку с нужным логином
    public User getUser(String login) {
        String query = " SELECT " +
                DataBaseCreator.User._ID + ", " +
                DataBaseCreator.User.USER_LOGIN + ", " +
                DataBaseCreator.User.USER_PASS + ", " +
                DataBaseCreator.User.USER_COUNTRY +
                " FROM " + DataBaseCreator.User.TABLE_USERS
                + " WHERE " + DataBaseCreator.User.USER_LOGIN + " = '" + login + "'";

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            return null;
        }  // если нет в списке login возврат null
        cursor.moveToFirst();
        User user = new User(cursor.getString(1), cursor.getString(2), cursor.getString(3));

        user.id = cursor.getInt(0);
        cursor.close();
        return user;
    }

    // таблица Region
    public long insertRegion(ResponseCountries responseCountries) {

        if (getAllRegion().contains(responseCountries.region)) {
            return -1;
        } else {
            ContentValues cv = new ContentValues();
            cv.put(DataBaseCreator.Region.REGION, responseCountries.region);
            return database.insert(DataBaseCreator.Region.TABLE_REGIONS, null, cv);
        }
    }

    public int getIdRegion(String region) {  // id региона по названию региона
        String query = " SELECT " +
                DataBaseCreator.Region._ID +
                " FROM " + DataBaseCreator.Region.TABLE_REGIONS
                + " WHERE " + DataBaseCreator.Region.REGION + " = '" + region + "'";

        Cursor cursor = database.rawQuery(query, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            return -1;
        }
        cursor.moveToFirst();
        int id = cursor.getInt(0);
        cursor.close();
        return id;
    }

    public String getRegionById(int idRegion) {  // регион по id региона
        String query = " SELECT " +
                DataBaseCreator.Region.REGION +
                " FROM " + DataBaseCreator.Region.TABLE_REGIONS
                + " WHERE " + DataBaseCreator.Region._ID + " = " + idRegion;

        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        String rez = cursor.getString(0);
        cursor.close();
        return rez;
    }

    public List<String> getAllRegion() {  // все регионы в списке
        String query = " SELECT " +
                DataBaseCreator.Region.REGION +
                " FROM " + DataBaseCreator.Region.TABLE_REGIONS;

        Cursor cursor = database.rawQuery(query, null);
        List<String> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
            //         Log.d("yad", "getAllRegion");
        }
        cursor.close();
        return list;
    }

    // таблица SubRegion
    public long insertSubRegion(ResponseCountries responseCountries) {
        if (getAllSubRegion().contains(responseCountries.subregion)) {
            return -1;
        } else {
            ContentValues cv = new ContentValues();
            cv.put(DataBaseCreator.SubRegion.SUBREGION, responseCountries.subregion);
            cv.put(REGION_ID, getIdRegion(responseCountries.region));
            return database.insert(DataBaseCreator.SubRegion.TABLE_SUBREGION, null, cv);
        }
    }

    public int getIdSubRegion(String subregion) {  // id субрегиона по названию субрегиона
        String query = " SELECT " +
                DataBaseCreator.SubRegion._ID +
                " FROM " + DataBaseCreator.SubRegion.TABLE_SUBREGION
                + " WHERE " + DataBaseCreator.SubRegion.SUBREGION + " = '" + subregion + "'";

        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        int id = cursor.getInt(0);
        cursor.close();
        return id;
    }

    public List<String> getAllSubRegion() {  // все регионы в списке
        String query = " SELECT " +
                DataBaseCreator.SubRegion.SUBREGION +
                " FROM " + DataBaseCreator.SubRegion.TABLE_SUBREGION;

        Cursor cursor = database.rawQuery(query, null);
        List<String> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
            //          Log.d("yad", "getAllSubRegion");
        }
        cursor.close();
        return list;
    }

    //таблица  Coutry--------------------------------------
    public long insertCountry(ResponseCountries responseCountries) {
        ContentValues cv = new ContentValues();
        cv.put(COUNTRY_NAME, responseCountries.name);
        cv.put(COUNTRY_CAPITAL, responseCountries.capital);
        cv.put(COUNTRY_POPULATION, responseCountries.population);
        cv.put(ID_SUBREGION, getIdSubRegion(responseCountries.subregion));
        return database.insert(DataBaseCreator.Country.TABLE_COUNTRY, null, cv);
    }

    // проверка наличия страны и возврат id субрегиона
    public int getIdSubRregionByCountry(String Country) {
        String query = "SELECT " + ID_SUBREGION +
                " FROM " + TABLE_COUNTRY +
                " WHERE " + COUNTRY_NAME + " = '" + Country + "'";
        Cursor cursor = database.rawQuery(query, null);
        if (cursor.getCount() == 0) {
            cursor.close();
            return -1;
        } else {
            cursor.moveToFirst();
            int id = cursor.getInt(0);
            cursor.close();
            return id;
        }
    }

    public List<ResponseCountries> getCountries(int subregion) {
        String query = " SELECT " +
                TABLE_COUNTRY + "." + _ID + ", " +
                COUNTRY_NAME + ", " +
                COUNTRY_CAPITAL + ", " +
                COUNTRY_POPULATION + ", " +
                ID_SUBREGION + ", " +
                REGION_ID +
                " FROM " + DataBaseCreator.SubRegion.TABLE_SUBREGION +
                " INNER JOIN " + DataBaseCreator.Country.TABLE_COUNTRY +
                " ON " + ID_SUBREGION + " = " +
                DataBaseCreator.SubRegion.TABLE_SUBREGION + "." + _ID;
        if (subregion > 0) { // если задан id субрегиона
            query = query + " WHERE " + ID_SUBREGION + " = " + subregion;
        }
        Cursor cursor = database.rawQuery(query, null);

        List<ResponseCountries> list = new ArrayList<>();

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            ResponseCountries country = new ResponseCountries();
            country.idBySQL = cursor.getInt(0);
            country.name = cursor.getString(1);
            country.capital = cursor.getString(2);
            country.population = cursor.getInt(3);
            country.subregion = cursor.getInt(4) + "";
            country.region = cursor.getInt(5) + "";

            list.add(country);
            cursor.moveToNext();
            //       Log.d("yad", "getCountry");
        }
        cursor.close();
        return list;
    }

    public ResponseCountries getCountryById(int idCountry) {
        String query = " SELECT " +
                COUNTRY_NAME + ", " +
                COUNTRY_CAPITAL + ", " +
                COUNTRY_POPULATION + ", " +
                ID_SUBREGION + ", " +
                REGION_ID +
                " FROM " + DataBaseCreator.SubRegion.TABLE_SUBREGION +
                " INNER JOIN " + DataBaseCreator.Country.TABLE_COUNTRY +
                " ON " + ID_SUBREGION + " = " +
                DataBaseCreator.SubRegion.TABLE_SUBREGION + "." + _ID +
                " WHERE " + TABLE_COUNTRY + "." + _ID + " = " + idCountry;

        Cursor cursor = database.rawQuery(query, null);
        cursor.moveToFirst();
        ResponseCountries country = new ResponseCountries();
        country.name = cursor.getString(0);
        country.capital = cursor.getString(1);
        country.population = cursor.getInt(2);
        country.subregion = cursor.getInt(3) + "";
        country.region = cursor.getInt(4) + "";
        cursor.close();
        return country;
    }

    //таблица  QuestionAndRezult--------------------------------------
    public long insertQuestion(int login, int idCountry, int quvestion) {
        ContentValues cv = new ContentValues();
        cv.put(QuestionAndRezult.LOGIN_ID, login);
        cv.put(QuestionAndRezult.COUNTRY_ID, idCountry);
        cv.put(QuestionAndRezult.QESTION_ID, quvestion);
        cv.put(QuestionAndRezult.REZULT, 0);
        return database.insert(QuestionAndRezult.TABLE_QUESTION, null, cv);
    }

    public long upDateQestionTable(int idQ, int rez) {
        ContentValues cv = new ContentValues();
        cv.put(QuestionAndRezult.REZULT, rez);
        String where = QuestionAndRezult._ID + " = " + idQ;
        return database.update(QuestionAndRezult.TABLE_QUESTION, cv, where, null);
    }

    public List<Question> getQuestionsForUser(int idLogin) {
        String query = "SELECT " +
                QuestionAndRezult._ID + ", " +
                QuestionAndRezult.LOGIN_ID + ", " +
                QuestionAndRezult.COUNTRY_ID + ", " +
                QuestionAndRezult.QESTION_ID + ", " +
                QuestionAndRezult.REZULT +
                " FROM " + QuestionAndRezult.TABLE_QUESTION +
                " WHERE " + QuestionAndRezult.LOGIN_ID + " = " + idLogin;
        Cursor cursor = database.rawQuery(query, null);
        List<Question> list = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Question question = new Question();
            question.idQuestion = cursor.getInt(0);
            question.idLogin = cursor.getInt(1);
            question.idCountry = cursor.getInt(2);
            question.numberQuestion = cursor.getInt(3);
            question.rezult = cursor.getInt(4);
            list.add(question);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }


}

