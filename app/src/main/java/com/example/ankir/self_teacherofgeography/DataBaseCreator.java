package com.example.ankir.self_teacherofgeography;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import static android.provider.BaseColumns._ID;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.*;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Country.TABLE_COUNTRY;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.QuestionAndRezult.*;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Region.REGION;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.Region.TABLE_REGIONS;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.SubRegion.REGION_ID;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.SubRegion.SUBREGION;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.SubRegion.TABLE_SUBREGION;
import static com.example.ankir.self_teacherofgeography.DataBaseCreator.User.*;


/**
 * Created by ankir on 13.06.2017.
 */

public class DataBaseCreator extends SQLiteOpenHelper {

    public static final String DB_NAME = "db_name";
    public static final int DB_VERSION = 1;
    DataBaseMaster dbMaster;

    public static class User implements BaseColumns {
        public static final String TABLE_USERS = "t_users";
        public static final String USER_LOGIN = "user_login";
        public static final String USER_PASS = "user_pass";
        public static final String USER_COUNTRY = "user_country";
    }

    public static class Region implements BaseColumns {
        public static final String TABLE_REGIONS = "table_regions";
        public static final String REGION = "region";
    }

    public static class SubRegion implements BaseColumns {
        public static final String TABLE_SUBREGION = "table_subregion";
        public static final String SUBREGION = "subregion";
        public static final String REGION_ID = "id_region";
    }


    public static class Country implements BaseColumns {
        public static final String TABLE_COUNTRY = "t_country";
        public static final String COUNTRY_NAME = "country_name";
        public static final String COUNTRY_CAPITAL = "country_capital";
        public static final String COUNTRY_POPULATION = "country_population";
        public static final String ID_SUBREGION = "id_subregion";
    }

    public static class QuestionAndRezult implements BaseColumns {
        public static final String TABLE_QUESTION = "t_question";
        public static final String LOGIN_ID = "login_id";
        public static final String COUNTRY_ID = "country_id";
        public static final String QESTION_ID = "question_id";
        public static final String REZULT = "rezult";
    }


    static String SCRIPT_CREATE_TBL_USER = "CREATE TABLE " + TABLE_USERS + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_LOGIN + " TEXT, " +
            USER_PASS + " TEXT, " +
            USER_COUNTRY + " TEXT" +
            ");";

    static String SCRIPT_CREATE_TBL_REGION = "CREATE TABLE " + TABLE_REGIONS + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            REGION + " TEXT" +
            ");";

    static String SCRIPT_CREATE_TBL_SUBREGION = "CREATE TABLE " + TABLE_SUBREGION + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            SUBREGION + " TEXT, " +
            REGION_ID + " INTEGER" +
            ");";

    static String SCRIPT_CREATE_TBL_COUNTRY = "CREATE TABLE " + TABLE_COUNTRY + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COUNTRY_NAME + " TEXT, " +
            COUNTRY_CAPITAL + " TEXT, " +
            COUNTRY_POPULATION + " INTEGER, " +
            ID_SUBREGION + " INEGER" +
            ");";

    static String SCRIPT_CREATE_TBL_QUESTION = "CREATE TABLE " + TABLE_QUESTION + " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            LOGIN_ID + " INEGER, " +
            COUNTRY_ID + " INEGER, " +
            QESTION_ID + " INEGER, " +
            REZULT + " INEGER" +
            ");";


    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCRIPT_CREATE_TBL_USER);
        Log.d("create tbl user", "ok");
        db.execSQL(SCRIPT_CREATE_TBL_REGION);
        Log.d("create tbl region", "ok");
        db.execSQL(SCRIPT_CREATE_TBL_SUBREGION);
        Log.d("create tbl subregion", "ok");
        db.execSQL(SCRIPT_CREATE_TBL_COUNTRY);
        Log.d("create tbl country", "ok");
        db.execSQL(SCRIPT_CREATE_TBL_QUESTION);
        Log.d("create tbl question", "ok");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //update, if need
        //1 через удаление базы
        //  db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        //  onCreate(db);

        //2 через добавление
        // db.execSQL("ALTER TABLE " + TABLE_NAME + " ADD " + User.USER_AGE + " INTEGER");
    }
}



