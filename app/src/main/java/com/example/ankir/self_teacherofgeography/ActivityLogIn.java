package com.example.ankir.self_teacherofgeography;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityLogIn extends AppCompatActivity {
    Md5 md5 = new Md5();
    Context context;
    DataBaseMaster dbMaster;
    Intent intent;
    @BindView(R.id.a_login)
    EditText login;
    @BindView(R.id.a_pass)
    EditText pass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.a_click_login)
    void onClickLogin() {
        dbMaster = DataBaseMaster.getInstance(this);
        User userCurrent = dbMaster.getUser(login.getText().toString());
        if (userCurrent == null) {
            Toast.makeText(ActivityLogIn.this, "нет такого пользователя", Toast.LENGTH_SHORT).show();
        } else if (userCurrent.pass.equals(md5.md5(pass.getText().toString()))) {
            ActivityQuestionnaire.currentLogin = userCurrent.id;

            this.onBackPressed();
        } else {
            Toast.makeText(ActivityLogIn.this, "неверный пароль", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.a_click_checin)
    void onClickCheckIn() {
        intent = new Intent(ActivityLogIn.this, ActivityCheckIn.class);
        startActivity(intent);
        this.onBackPressed();
    }


}
